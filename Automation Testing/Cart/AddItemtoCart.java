package Cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 */
public class AddItemtoCart {
     private WebDriver driver;

     //Search item
     By searchItem = By.id("search_query_top");
     //Search button
     By searchButton = By.name("submit_search");

     //Select product
     //Click on product selected
     By product = By.xpath("//*[@id=\"center_column\"]/ul/li[4]/div/div[1]/div/a[1]/img");

     //Left quantity in "1"
     //Select size --------- Value= "2"
     Select size = new Select(driver.findElement(By.name("group_1")));
     //Select color
     By color = By.id("color_16");

     //Click button ADdd to Cart
     By AddtoCartButton = By.className("exclusive");

     //Product shown successfully

     //Initialize constructor
     public AddItemtoCart(WebDriver driver){
          this.driver = driver;
     }

     public void AddItem() {
          driver.findElement(searchItem).sendKeys("Dress");
          driver.findElement(searchButton).click();

          driver.findElement(product).click();
          size.selectByValue("2");
          driver.findElement(color).click();

          driver.findElement(AddtoCartButton).click();
     }
}
