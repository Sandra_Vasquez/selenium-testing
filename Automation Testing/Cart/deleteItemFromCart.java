/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package Cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class deleteItemFromCart {
     private WebDriver driver;

     //CartButton
     By cartButton = By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a");

     //Choose item to delete
     //Delete item
     By deleteButton = By.id("7_34_0_0");


     //Initialize constructor
     public deleteItemFromCart(WebDriver driver) {
          this.driver = driver;
     }

     //Run process
     public void deleteItem(){
          //Go to cart
          driver.findElement(cartButton).click();
          //Delete item
          driver.findElement(deleteButton).click();
          //Item deleted
     }
}
