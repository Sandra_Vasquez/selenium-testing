package Cart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

/**
 *
 */
public class deleteItemFromCartTest {
     @Test
     public void testDeleteItem(){

          System.setProperty("webdriver.gecko.driver",
              "/Users/Sandra Vasquez/Downloads/geckodriver.exe");
          WebDriver driver = new FirefoxDriver();
          driver.manage().window().maximize();
          driver.get("http://automationpractice.com/index.php");

          //Pass driver to deleteItemFromCart class
          deleteItemFromCart delete = new deleteItemFromCart(driver);

          delete.deleteItem();

          driver.quit();
     }
}
