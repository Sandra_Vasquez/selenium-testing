/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package FilterAndSearchItem;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 */
public class FilterItemFromCategories {
    WebDriver driver;

    //Categories
    By categoriesButton = By.xpath("//*[@id=\"block_top_menu\"]/div");
    //Dresses
    By dressesButton = By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a");
    //Catalog
    By catalogButton = By.xpath("//*[@id=\"layered_block_left\"]");
    //Choose color
    Select color = new Select(driver.findElement(By.name("layered_id_attribute_group_11")));

    public FilterItemFromCategories(WebDriver driver) {
        this.driver = driver;
    }

    public void FilterProcess() {
        driver.findElement(categoriesButton).click();
        driver.findElement(dressesButton).click();
        driver.findElement(catalogButton).click();
        color.selectByVisibleText("Black");
    }

}
