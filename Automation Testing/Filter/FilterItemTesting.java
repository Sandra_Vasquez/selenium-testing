package FilterAndSearchItem;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

/**
 *
 */
public class FilterItemTesting {
     @Test
     public void ValidateFilter() {
          WebDriver driver = new FirefoxDriver();
          driver.manage().window().maximize();
          driver.get("http://automationpractice.com/index.php");

          FilterItemFromCategories filter = new FilterItemFromCategories (driver);

          filter.FilterProcess();

          driver.quit();
     }
}
