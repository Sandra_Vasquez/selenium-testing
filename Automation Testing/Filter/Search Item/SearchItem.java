package FilterAndSearchItem;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class SearchItem {
    WebDriver driver;

    By searchLabel = By.name("search_query");
    By searchButton = By.name("submit_search");

    public SearchItem(WebDriver driver) {
        this.driver = driver;
    }

    public void SearchItemValidation(String Item){
        driver.findElement(searchLabel).sendKeys(Item);
        driver.findElement(searchButton).click();
    }
}
