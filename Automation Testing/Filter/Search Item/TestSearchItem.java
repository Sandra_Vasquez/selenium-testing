package FilterAndSearchItem;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

/**
 *
 */
public class TestSearchItem {
    @Test
    public void FindItem() {
        WebDriver driver = new FirefoxDriver();

        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

        SearchItem search = new SearchItem(driver);

        search.SearchItemValidation("Blouse");

        driver.quit();
    }
}
