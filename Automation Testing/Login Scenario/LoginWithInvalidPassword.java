package Login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class LoginWithInvalidPassword {
     WebDriver driver;

     By email = By.name("email");
     By pass = By.id("passwd");
     By submitButton = By.name("SubmitLogin");

     //Create constructor
     public LoginWithInvalidPassword(WebDriver driver) {
          this.driver = driver;
     }

     public void LoginValidation(String emailLogin, String password) {
          driver.findElement(email).sendKeys(emailLogin);
          driver.findElement(pass).sendKeys(password);
          driver.findElement(submitButton).click();
     }
}
