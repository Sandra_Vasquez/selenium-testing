package Login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 */
public class SignUp {
     //driver variable initiated
     WebDriver driver;

     //Create constructor
     public SignUp(WebDriver driver) {
          this.driver = driver;
     }

     //Sign up button
     By signupButton = By.xpath("/html/body/div/div[1]/header/div[2]/div/div/nav/div[1]/a");
     //Label for email registering
     By emailSignUp = By.name("email_create");
     //Create an account button
     By createAccountButton = By.name("SubmitCreate");
     //Radio button
     By titleButton = By.id("uniform-id_gender2");
     //First name label
     By firstName = By.name("customer_firstname");
     //Last name label
     By lastName = By.id("customer_lastname");
     //Password label
     By password = By.id("passwd");

     //Date of birth
     //Day
     Select dayOfBirth = new Select(driver.findElement(By.name("days")));
     //Month
     Select monthOfBirth = new Select(driver.findElement(By.name("months")));
     //Year
     Select yearOfBirth = new Select(driver.findElement(By.name("years")));

     //Address
     By address = By.id("address1");
     //City
     By city = By.id("city");
     //State
     Select state = new Select(driver.findElement(By.id("id_state")));
     //zip
     By zip = By.name("postcode");
     //Country
     Select country = new Select(driver.findElement(By.id("id_country")));

     //Additional Information
     By additionalInfo = By.id("other");
     //Mobile phone
     By mobilePhone = By.name("phone_mobile");
     //Alias
     By alias = By.name("alias");

     //Register button
     By registerButton = By.id("submitAccount");

     //Send data to page
     public void SignUpProcess(String email, String firstN, String lastN, String passw, String a_ddress, String citY, String PostalCode, String addInfo, String phone, String Alias) {
          //First interface
          driver.findElement(signupButton).click();
          //Second Interface
          driver.findElement(emailSignUp).sendKeys(email);
          driver.findElement(createAccountButton).click();
          //Third Interface
          //Personal Information
          driver.findElement(titleButton).click();
          driver.findElement(firstName).sendKeys(firstN);
          driver.findElement(lastName).sendKeys(lastN);
          driver.findElement(password).sendKeys(passw);
          //Date of birth
          dayOfBirth.selectByValue("28");
          monthOfBirth.selectByVisibleText("August");
          yearOfBirth.selectByValue("1996");
          //Address
          driver.findElement(address).sendKeys(a_ddress);
          driver.findElement(city).sendKeys(citY);
          state.selectByVisibleText("Texas");
          driver.findElement(zip).sendKeys(PostalCode);
          country.selectByValue("21");
          driver.findElement(additionalInfo).sendKeys(addInfo);
          driver.findElement(mobilePhone).sendKeys(phone);
          driver.findElement(alias).sendKeys(Alias);
          driver.findElement(registerButton).click();
     }
}
