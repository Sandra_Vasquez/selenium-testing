package Login;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 */
public class SignUpTesting {

    @Test
    public void SignUpTesting() {
        WebDriver driver = new FirefoxDriver();
        //Maximize window
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

        //Passing driver to SignUp class
        SignUp signup = new SignUp(driver);

        //Send data to SignUp class
        signup.SignUpProcess("testQA@example.com", "QAuser", "Automation", "Pass123*", "53 Any Street", "Kansas", "66062", "Black house", "1234567890", "My home");

        driver.quit();
    }
}
