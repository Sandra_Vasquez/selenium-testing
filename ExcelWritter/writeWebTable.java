import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author Sandra Vasquez
 */
public class writeWebTable {
@Test
    public void ExcelWritter() throws IOException, InterruptedException {
        //Set driver
        System.setProperty("webdriver.chrome.driver",
            "/Users/Sandra Vasquez/Downloads/chromedriver.exe");
        WebDriver chrome = new ChromeDriver();
        chrome.manage().window().maximize();
        chrome.get("https://cosmocode.io/automation--practice-webtable/");
        Thread.sleep(5000);

        //Set file
         File file = new File("/Users/Sandra Vasquez/Downloads/WebTable.xlsx");
         XSSFWorkbook workbook = new XSSFWorkbook();
         XSSFSheet sheet = workbook.createSheet("WebTable Practice");

        //Find table element
        WebElement table = chrome.findElement(By.id("countries"));

        //Get Rows
        List<WebElement> totalRows = table.findElements(By.tagName("tr"));

        //Count rows
        for (int row = 0; row < totalRows.size(); row++) {
            XSSFRow rowValue = sheet.createRow(row);

            //Get columns
            List<WebElement> totalColumns = totalRows.get(row).findElements(By.tagName("td"));

            //Count columns
            for (int col = 0; col < totalColumns.size(); col++) {
                String cellValue =  totalColumns.get(col).getText();
                System.out.print(cellValue + "\t");
                rowValue.createCell(col).setCellValue(cellValue);
            }
            System.out.println();
        }
        FileOutputStream fos = new FileOutputStream(file);
        workbook.write(fos);
        workbook.close();
    }
}
