package com.wordpress.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class LoginPage {

    WebDriver driver;

    By username = By.id("user_login");
    By password = By.xpath(".//*[@id='user_pass']");
    By loginButton = By.name("wp-submit");

    //Create constructor
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
//Option 1
    public void LoginToWordpress(String userid,String pass) {
        driver.findElement(username).sendKeys(userid);
        driver.findElement(password).sendKeys(pass);
        driver.findElement(loginButton).click();
    }
//option 2
    //Create methods
    public void typeUserName(String uid) {

        driver.findElement(username).sendKeys(uid);

    }

    public void typePassword(String pass) {

        driver.findElement(password).sendKeys(pass);

    }

    public void clickOnButton() {

        driver.findElement(loginButton).click();
    }

}
