package com.wordpress.Testcases;

import com.wordpress.Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 */
public class VerifyWordpressLogin {

     @Test
     public void verifyValidLogin() {

          WebDriver driver = new FirefoxDriver();
          driver.manage().window().maximize();
          driver.get("http://demosite.center/wordpress/wp-login.php");

          //Passing driver to LoginPage.java
          LoginPage login = new LoginPage(driver);


          //for option 1
          login.LoginToWordpress("admin", "demo123");

          //for option 2
          login.typeUserName("admin");
          login.typePassword("demo123");
          login.clickOnButton();

          //Close driver
          driver.quit();

     }
}
