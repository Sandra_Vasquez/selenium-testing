import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class dragAndDrop {
    public static void main(String[] args) {
        /*
        How do you implement drag and drop?

        Drag and drop is an action performed by the mouse
        to move an element into another area.

        For example, when attaching a file into your email,
        you can drag the file from a folder to the email window.
         */

        System.setProperty("webdriver.chrome.driver",
            "/Users/Sandra Vasquez/Downloads/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/dragdrop");


        //Locate elements and set variable names
        WebElement image = driver.findElement(By.id("image"));
        WebElement box = driver.findElement(By.id("box"));

        //Initiate Actions
        Actions action = new Actions(driver);

        //Perform drag and drop action by calling elements
        action.dragAndDrop(image, box).build().perform();

        driver.quit();

    }
}
