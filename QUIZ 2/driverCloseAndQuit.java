/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 */
public class driverCloseAndQuit {
    public void drivers() {
        /*
        What is the difference between driver.close() and driver.quit()?

        Both methods are designed to close the browser instance.
        driver.close() method is to close the current open window from the browser tested.
        so, if there are more than one browser window opened, it will close the current window.

        driver.quit() method will close all browser windows used for testing.
         */
        System.setProperty("webdriver.chrome.driver",
            "/Users/Sandra Vasquez/Downloads/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

        //Do some interaction
        //Click on sign in button
        //Look for an item

        //Close current window
        driver.close();
        //Close browser
        driver.quit();
    }
}
