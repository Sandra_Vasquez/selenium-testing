import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class implicitAndExplicitWait {
    public void waits() {
        /*
        Which one is preferred, implicit wait or explicit wait?

        Explicit wait provide better options than implicit wait because its
        dynamical loading with elements. This kind of wait must receive a
        condition in order to provide the developer results once the condition
        is satisfied.
        It applies for a particular scenario.
         */

        System.setProperty("webdriver.chrome.driver",
            "/Users/Sandra Vasquez/Downloads/chroedriver.exe");
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.get("https://myapp.tcs.com");

        By name = By.name("login");
        By pass = By.id("passwd");
        By loginBtn = By.id("loginBtn");

        driver.findElement(name).sendKeys("id_employee");
        driver.findElement(pass).sendKeys("password");
        driver.findElement(loginBtn).click();


    }
}
