import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class staleElementException {
    public void staleElement() {
        WebDriver driver = new ChromeDriver();


        /*
         * What is staleElementException in Selenium?
         *
         * staleElement exception is a WebDriver error thrown because an element is no longer available,
         * it does not exist, or it is old.
         * There are four solution for this error:
         */

        //1. Refreshing webpage
        driver.navigate().refresh();
        driver.findElement(By.xpath("xpath locator")).click();

        //2. Using try catch block
        try {
            driver.findElement(By.xpath("xpath locator")).click();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //3. Using ExpectedConditions.refreshed
        //Wait for elements until they are available
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("xpath locator")));

        //4. By applying "lazy initialization" on POM.xml by using the following method
        initElements();
    }
}
