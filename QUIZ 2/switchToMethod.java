import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.swing.text.html.StyleSheet;
import java.util.Iterator;
import java.util.Set;

public class switchToMethod {
    /*
    Explain in what 3 scenarios driver.switchTo() method can be used.

    switchTo() method is used to interact between different frames or windows.

     */
    public void switchTo() {
        System.setProperty("webdriver.chrome.driver",
            "/Users/Sandra Vasquez/Downloads/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/browser-windows");

        //Open child windows
        driver.findElement(By.id("windowButton")).click();
        driver.findElement(By.id("messageWindowButton")).click();

        //Store main window
        String parentWindow = driver.getWindowHandle();

        //Handle all opened windows
        //All child windows will be stored in a set of strings
        Set<String> s = driver.getWindowHandles();
        //For iteration between all child windows
        Iterator<String> i = s.iterator();


        //Check if child window has more child windows to close them
        while (i.hasNext()) {
            String childWindow = i.next();
            //Here it will close those windows that are not the parent window
            if (!parentWindow.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
                //Close child window
                driver.close();
            }
        }

        //Switch back to parent window
        driver.switchTo().window(parentWindow);
        //Close main window
        driver.quit();

    }
}
