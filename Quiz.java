import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class SeleniumQuiz {
    public static void main(String[] args) {
        //Selenium Quiz

        /*
        What are different type element locators of Selenium?

        Selenium uses eight different locators:
        By ID, name, className, tagName, linkText, partialLinkText, cssSelector, and Xpath.
        The most common is by using ID, and the less recommended is by using the xpath locator.
         */

        System.setProperty("webdriver.chrome.driver", "/Users/Sandra Vasquez/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();

        //Set page for testing
        driver.get("https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&ct=1631913719&rver=7.0.6737.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f0%2f%3fstate%3d1%26redirectTo%3daHR0cHM6Ly9vdXRsb29rLmxpdmUuY29tL21haWwvMC9pbmJveC8%26RpsCsrfState%3dbb8db5a3-231e-f2ae-e0e3-4bb001750d8b&id=292841&aadredir=1&whr=hotmail.com&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015");

        //Using ID locator
        WebElement button = driver.findElement(By.id("idSIButton9"));
        button.click();

        //Using xpath locator
        WebElement sameButton = driver.findElement(By.xpath("//*[@id=\"idSIButton9\"]"));
        sameButton.click();

        driver.quit();

        /*
        What is the difference between findElement and findElements?

        findElement returns just one WebElement.
        findElements works with a list of WebElements.
         */

        WebDriver driver2 = new ChromeDriver();
        driver2.get("https://www.google.com/");

        //findElement
        WebElement search = driver2.findElement(By.name("q"));
        search.sendKeys("Hello World");

        //findElements
        List<WebElement> box = driver2.findElements(By.className("LX3sZb"));
        box.size();

        driver2.quit();

        /*
        Difference between absolute and relative XPATH

        The absolute XPATH contains the whole route or path beginning from the root.
        The relative XPATH starts from the element that we look for testing, starting with the '//' symbol.
         */
        WebDriver driver3 = new ChromeDriver();
        driver3.get("https://github.com/");

        //Absolute XPATH
        WebElement xpath1 = driver3.findElement(By.xpath("/html/body/div[4]/main/div[1]/div[1]/div[1]/div/div/div[1]/form/div/dl/dd/input"));
        xpath1.click();

        //Relative XPATH
        WebElement xpath2 = driver3.findElement(By.xpath("//*[@id=\"user_email\"]"));
        xpath2.sendKeys("example@gmail.com");

        driver3.quit();

        /*
        What is XPath axes tag with example

        Are methods designed to identify dynamic elements which are not possible to find
        by a normal XPath (using ID, name, class name, tag name, etc.).
        There are 13 different axes used in Selenium.
        */

        WebDriver driverXpath = new ChromeDriver();
        driverXpath.get("www.amazon.com.mx/");

        WebElement xpathAxes = driverXpath.findElement(By.xpath("//input[@id='twotabsearchtextbox']//ancestor::div"));
        xpathAxes.sendKeys("Clothe");

        driverXpath.quit();

        /*
        How to select dropdown using Selenium?
         */
        //1. Set web resource
        WebDriver driverDropdown = new ChromeDriver();
        driverDropdown.get("https://www.w3schools.com/");

        //2. Select the dropdown element by ID locator
        WebElement dropdown = driverDropdown.findElement(By.id("navbtn_tutorials"));
        dropdown.click();

        //3. Choose any option from the dropdown menu by XPath locator
        WebElement chooseElement = driverDropdown.findElement(By.xpath("//*[@id=\"sectionxs_tutorials\"]/div/div/div[2]/div/a[17]"));
        chooseElement.click();
        
        driverDropdown.quit();


    }
}

